package com.upgrade.island.rest;

import com.upgrade.island.BookingService;
import com.upgrade.island.BookingServiceImpl;
import com.upgrade.island.dao.Reservation;
import com.upgrade.island.dao.ReservationDaoImpl;
import com.upgrade.island.rest.response.AvailabilityResponse;
import com.upgrade.island.rest.response.ReservationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for BookingServiceImpl.
 */
@RestController
public class BookingServiceController {

    @Autowired
    private BookingService bookingService;

    @GetMapping("/availability")
    AvailabilityResponse availability() {
        AvailabilityResponse r = new AvailabilityResponse();
        try {
            r.setAvailability(bookingService.getAvailability());
        } catch (IllegalArgumentException e) {
            r.setError(e.getMessage());
        }
        return r;
    }

    @GetMapping("/reservation/get/{id}")
    ReservationResponse get(@PathVariable String id) {
        ReservationResponse r = new ReservationResponse();
        try {
            r.setReservation(bookingService.get(id));
        } catch (IllegalArgumentException e) {
            r.setError(e.getMessage());
        }
        return r;
    }

    @DeleteMapping("/reservation/cancel/{id}")
    ReservationResponse cancel(@PathVariable String id) {
        ReservationResponse r = new ReservationResponse();
        try {
            bookingService.cancel(id);
        } catch (IllegalArgumentException e) {
            r.setError(e.getMessage());
        }
        return r;
    }

    @PostMapping("/reservation/update/{id}")
    ReservationResponse update(@RequestBody Reservation r, @PathVariable String id) {
        ReservationResponse response = new ReservationResponse();
        try {
            bookingService.update(id, r.getEmail(), r.getName(), r.getArrival(), r.getDeparture());
        } catch (IllegalArgumentException e) {
            response.setError(e.getMessage());
        }
        return response;
    }

    @PostMapping("/reservation/create")
    ReservationResponse update(@RequestBody Reservation r) {
        ReservationResponse response = new ReservationResponse();
        try {
            bookingService.create(r.getEmail(), r.getName(), r.getArrival(), r.getDeparture());
        } catch (IllegalArgumentException e) {
            response.setError(e.getMessage());
        }
        return response;
    }
}
