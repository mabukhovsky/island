package com.upgrade.island.rest.response;

import java.time.LocalDate;
import java.util.Set;

/**
 * REST API: response for get Availability request.
 */
public class AvailabilityResponse extends Response {

    private Set<LocalDate> availability;

    public void setAvailability(Set<LocalDate> availability) {
    }

    public Set<LocalDate> getAvailability() {
        return availability;
    }
}
