package com.upgrade.island.rest.response;

import com.upgrade.island.dao.Reservation;

/**
 * Response for Create \ Update \ Cancel reservation request.
 */
public class ReservationResponse extends Response {

    private Reservation reservation;

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Reservation getReservation() {
        return reservation;
    }
}
