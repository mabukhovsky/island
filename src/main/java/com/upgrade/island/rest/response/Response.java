package com.upgrade.island.rest.response;


public class Response {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
