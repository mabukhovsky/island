package com.upgrade.island;

import com.upgrade.island.dao.Reservation;
import com.upgrade.island.dao.ReservationDao;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.awt.print.Book;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Booking service implementation.
 */
@Component("bookingService")
public class BookingServiceImpl implements BookingService {

    @Autowired
    private final ReservationDao reservationDao;

    /**
     * Set of available dates that:
     * 1. Is thread-safe for concurrent usage
     * 2. Maintains ascending order of dates
     * 3. Insert / remove / add complexity: O(log2(n)), n is limited by max number of days in one month (31)
     *
     * JavaDoc: A scalable concurrent NavigableSet implementation based on a ConcurrentSkipListMap. The elements of the set are kept sorted
     * according to their natural ordering, or by a Comparator provided at set creation time, depending on which constructor is used.
     * This implementation provides expected average log(n) time cost for the contains, add, and remove operations and their variants.
     * Insertion, removal, and access operations safely execute concurrently by multiple threads.
     */
    private final Set<LocalDate> availableDates = new ConcurrentSkipListSet<>((d1, d2) -> d1.isEqual(d2) ? 0 : (d1.isAfter(d2) ? 1 : -1));

    /**
     * Initializes booking service based on existing reservations in database.
     */
    public BookingServiceImpl(ReservationDao reservationDao) {
        this.reservationDao = reservationDao;
        LocalDate availableDate = oneDayAfter();
        LocalDate oneMonthAfter = oneMonthAfter();
        // initialize availableDates with all dates within month
        while(availableDate.isBefore(oneMonthAfter) || availableDate.isEqual(oneMonthAfter)) {
            availableDates.add(availableDate);
            availableDate = availableDate.plusDays(1);
        }
        // exclude dates that are already reserved
        List<Reservation> reservations = reservationDao.getAll();
        for(Reservation r: reservations) {
            modifyAvailabilityDates(r, false);
        }
    }

    /**
     * Every midnight - shift availability dates (exclude today, add next day of the following month).
     */
    @Scheduled(cron = "0 0 0 * * *")
    public void shiftAvailabilityDates() {
        // getToday() is referring a brand new day that must be excluded
        availableDates.remove(getToday());
        // oneMonthAfter() is referring a new day one month after that must be included
        availableDates.add(oneMonthAfter());
    }

    @Override
    public Set<LocalDate> getAvailability() {
        return availableDates;
    }

    @Override
    public Reservation create(String email, String name, LocalDate arrival, LocalDate departure) {
        validateReservationParameters(email, name, arrival, departure);
        Reservation reservation = new Reservation(email, name, arrival, departure);
        modifyAvailabilityDates(reservation, false);
        reservation = reservationDao.save(reservation);
        return reservation;
    }

    /**
     * User updates existing reservation.
     *
     * @param id ID of existing reservation
     * @param email must be non-null and not empty.
     * @param name must be non-null and not empty.
     * @param arrival arrival time, must be in range [1m before, 1d before] comparing to current date.
     * @param departure departure time, must be in range [arrival + 1d, arrival + 3d].
     *
     * @return updated reservation
     */
    @Override
    public Reservation update(String id, String email, String name, LocalDate arrival, LocalDate departure) {
        validateReservationParameters(email, name, arrival, departure);
        Reservation existing = reservationDao.getById(id);
        if(existing == null) {
            throw new IllegalArgumentException(String.format(ERROR_NO_RESERVATION_WITH_ID, id));
        }
        modifyAvailabilityDates(existing, true);
        Reservation updatedReservation = new Reservation(existing.getId(), email, name, arrival, departure);
        modifyAvailabilityDates(updatedReservation, false);
        reservationDao.update(updatedReservation);
        return updatedReservation;
    }

    /**
     * User cancels existing reservation.
     *
     * @param id ID of reservation.
     */
    @Override
    public void cancel(String id) {
        Reservation existing = get(id);
        modifyAvailabilityDates(existing, true);
        reservationDao.remove(id);
    }

    /**
     * User requests details of reservation.
     *
     * @param id ID of reservation.
     */
    @Override
    public Reservation get(String id) {
        if(id == null || id.isEmpty()) {
            throw new IllegalArgumentException(ERROR_RESERVATION_ID_NULL_OR_EMPTY);
        }
        Reservation existing = reservationDao.getById(id);
        if(existing == null) {
            throw new IllegalArgumentException(String.format(ERROR_NO_RESERVATION_WITH_ID, id));
        }
        return existing;
    }

    private void validateReservationParameters(String email, String name, LocalDate arrival, LocalDate departure) {
        validateString(name, ARG_NAME);
        validateString(email, ARG_EMAIL);
        if(!EmailValidator.getInstance().isValid(email)) {
            throw new IllegalArgumentException(String.format(ERROR_INVALID_EMAIL, email));
        }
        validateDate(arrival, ARG_ARRIVAL);
        validateDate(departure, ARG_DEPARTURE);
        LocalDate oneMonthAfter = oneMonthAfter();
        LocalDate oneDayAfter = oneDayAfter();
        if(arrival.isAfter(oneMonthAfter) || arrival.isBefore(oneDayAfter)) {
            throw new IllegalArgumentException(ERROR_ARRIVAL_RANGE);
        }
        if(!departure.isAfter(arrival) || ChronoUnit.DAYS.between(arrival, departure) > 3) {
            throw new IllegalArgumentException(ERROR_DEPARTURE_RANGE);
        }
        LocalDate current = arrival;
        while(current.isBefore(departure)) {
            if (!availableDates.contains(current)) {
                throw new IllegalArgumentException(String.format(ERROR_UNAVAILABLE_DATE, current));
            }
            current = current.plusDays(1);
        }
    }

    private void modifyAvailabilityDates(Reservation r, boolean include) {
        if(include) {
            availableDates.addAll(r.getBookedDays());
        } else {
            availableDates.removeAll(r.getBookedDays());
        }
    }

    private void validateString(String s, String title) {
        if(s == null || s.isEmpty()) {
            throw new IllegalArgumentException(title + ERROR_STRING_IS_NULL_OR_EMPTY);
        }
    }

    private void validateDate(LocalDate d, String title) {
        if(d == null) {
            throw new IllegalArgumentException(title + ERROR_DATE_IS_NULL);
        }
    }

    private LocalDate getToday() {
        ZoneId zoneId = ZoneId.of(TIME_ZONE) ;
        return LocalDate.now(zoneId);
    }

    private LocalDate oneMonthAfter() {
        return getToday().plusMonths(1);
    }

    private LocalDate oneDayAfter() {
        return getToday().plusDays(1);
    }
}
