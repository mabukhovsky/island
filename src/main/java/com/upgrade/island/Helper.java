package com.upgrade.island;

import java.util.UUID;

/**
 * Helper class with common functions.
 */
public class Helper {

    public static String getRandomId() {
        return UUID.randomUUID().toString();
    }
}
