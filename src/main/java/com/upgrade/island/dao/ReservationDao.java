package com.upgrade.island.dao;

import java.util.List;

public interface ReservationDao {

    Reservation save(Reservation reservation);

    Reservation getById(String id);

    Reservation update(Reservation reservation);

    void remove(String id);

    List<Reservation> getAll();
}
