package com.upgrade.island.dao;

import com.upgrade.island.Helper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is a stub class for in memory DAO implementation.
 */
@Component("reservationDao")
public class ReservationDaoImpl implements ReservationDao {

    private final Map<String, Reservation> data = new HashMap<>();

    public ReservationDaoImpl() {
    }

    /**
     * Saves reservation to database and returns a clone of it with initialized ID.
     */
    @Override
    public Reservation save(Reservation r) {
        Reservation rdb = new Reservation(Helper.getRandomId(), r.getName(), r.getEmail(), r.getArrival(), r.getDeparture());
        data.put(rdb.getId(), rdb);
        return rdb;
    }

    @Override
    public Reservation getById(String id) {
        return data.get(id);
    }

    @Override
    public Reservation update(Reservation reservation) {
        data.put(reservation.getId(), reservation);
        return reservation;
    }

    @Override
    public void remove(String id) {
        data.remove(id);
    }

    @Override
    public List<Reservation> getAll() {
        return data.values().stream()
                .collect(Collectors.toList());
    }
}
