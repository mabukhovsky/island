package com.upgrade.island.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Reservation of a campsite on a small island.
 * Immutable (thread-safe).
 *
 * @author mabukhovsky
 */
public class Reservation {

    private final String id;

    private final String name;

    private final String email;

    private final LocalDate arrival;

    private final LocalDate departure;

    public Reservation(String id, String name, String email, LocalDate arrival, LocalDate departure) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.arrival = arrival;
        this.departure = departure;
    }

    public Reservation(String email, String name, LocalDate arrival, LocalDate departure) {
        this.id = null;
        this.name = name;
        this.email = email;
        this.arrival = arrival;
        this.departure = departure;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getArrival() {
        return arrival;
    }

    public LocalDate getDeparture() {
        return departure;
    }

    /**
     * @return list of days for reservation. Reservation for 1 day will return arrival day only.
     * Reservation for 2 days will contain arrival and the next day etc.
     */
    public List<LocalDate> getBookedDays() {
        List<LocalDate> bookedDays = new LinkedList();
        LocalDate current = arrival;
        while(current.isBefore(departure)) {
            bookedDays.add(current);
            current = current.plusDays(1);
        }
        return bookedDays;
    }

    @Override
    public String toString() {
        return "id: " + id + " | email: " + email + " | name: " + name + " | arrival: " + arrival + " | departure: " + departure;
    }
}
