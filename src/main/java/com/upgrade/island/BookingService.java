package com.upgrade.island;

import com.upgrade.island.dao.Reservation;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * Interface for booking service.
 *
 * @author Maxim Abukhovsky
 */
public interface BookingService {

    String ERROR_NO_RESERVATION_WITH_ID = "Reservation with ID %s doesn't exist.";
    String ERROR_RESERVATION_ID_NULL_OR_EMPTY = "Reservation ID can't be null or empty.";
    String ERROR_ARRIVAL_RANGE = "Arrival should be in range [today + 1d, today + 1m].";
    String ERROR_DEPARTURE_RANGE = "Departure should be in range [arrival + 1d, arrival + 3d]";
    String ERROR_UNAVAILABLE_DATE = "The following date is unavailable: %s";
    String ERROR_INVALID_EMAIL = "Invalid email: %s";

    String ERROR_STRING_IS_NULL_OR_EMPTY = " can't be null or empty.";
    String ERROR_DATE_IS_NULL = " can't be null.";

    String ARG_NAME = "Name";
    String ARG_EMAIL = "Email";
    String ARG_ARRIVAL = "Arrival";
    String ARG_DEPARTURE = "Departure";

    String TIME_ZONE = "America/Los_Angeles";

    /**
     * High load call expected on this method.
     *
     * @return List of LocalDate available for booking.
     */
    Set<LocalDate> getAvailability();

    /**
     * Creates reservation.
     *
     * @param email must be non-null and not empty.
     * @param name must be non-null and not empty.
     * @param arrival arrival time, must be in range [1m before, 1d before] comparing to current date.
     * @param departure departure time, must be in range [arrival + 1d, arrival + 3d].
     *
     * @return Reservation in case of success.
     *
     * @throws IllegalArgumentException in case of failure or a wrong argument.
     */
     Reservation create(String email, String name, LocalDate arrival, LocalDate departure);

    /**
     * Updates existing reservation.
     *
     * @param id ID of existing reservation
     * @param email must be non-null and not empty.
     * @param name must be non-null and not empty.
     * @param arrival arrival time, must be in range [1m before, 1d before] comparing to current date.
     * @param departure departure time, must be in range [arrival + 1d, arrival + 3d].
     *
     * @return updated Reservation data in case of success.
     *
     * @throws IllegalArgumentException in case of failure or a wrong argument.
     */
     Reservation update(String id, String email, String name, LocalDate arrival, LocalDate departure);

    /**
     * Cancel existing reservation.
     *
     * @param id ID of reservation.
     *
     * @throws IllegalArgumentException in case of failure or a wrong argument.
     */
     void cancel(String id);

    /**
     * Get existing reservation details.
     *
     * @param id ID of reservation.
     *
     * @throws IllegalArgumentException in case of failure or a wrong argument.
     */
     Reservation get(String id);
}
