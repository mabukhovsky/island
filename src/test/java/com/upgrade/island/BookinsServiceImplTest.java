package com.upgrade.island;

import com.upgrade.island.dao.Reservation;
import com.upgrade.island.dao.ReservationDao;
import com.upgrade.island.dao.ReservationDaoImpl;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import static org.springframework.test.context.transaction.TestTransaction.start;

/**
 * Tests for BookingServiceImpl - covers most of the cases.
 */
public class BookinsServiceImplTest {

    private final String TIMEZONE = "America/Los_Angeles";
    private final String EMAIL_VALID = "email@gmail.com";
    private final String EMAIL_INVALID = "email-gmail.com";

    private final String NAME_VALID = "Arthur";
    private final String NAME_INVALID = "";

    private final int DAYS_FROM_NOW = 4;

    /**
     * By default we do not expect exceptions (POSITIVE cases).
     * Only NEGATIVE tests should change this value.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * DB with no reservations.
     */
    private final ReservationDao dbNoReservations = new ReservationDaoImpl();

    /**
     * DB with odd days reservations.
     */
    private final ReservationDao dbOddDaysReservation = new ReservationDaoImpl() {
        @Override
        public List<Reservation> getAll() {
            LocalDate date = getToday();
            List<Reservation> reservations = new LinkedList<>();
            LocalDate oneMonthLater = date.plusDays(31);
            while (date.isBefore(oneMonthLater)) {
                if (date.getDayOfMonth() % 2 == 1) {
                    Reservation r = new Reservation(Helper.getRandomId(), "UserName", "UserEmail", date, date.plusDays(1));
                    reservations.add(r);
                }
                date = date.plusDays(1);
            }
            return reservations;
        }
    };

    /**
     * POSITIVE: Verifies all dates are available for whole month if no reservations exist in DB.
     * <p>
     * 2020-11-12 | 2020-11-13 | 2020-11-14 | 2020-11-15 | 2020-11-16 | 2020-11-17 | 2020-11-18 |
     * 2020-11-19 | 2020-11-20 | 2020-11-21 | 2020-11-22 | 2020-11-23 | 2020-11-24 | 2020-11-25 |
     * 2020-11-26 | 2020-11-27 | 2020-11-28 | 2020-11-29 | 2020-11-30 | 2020-12-01 | 2020-12-02 |
     * 2020-12-03 | 2020-12-04 | 2020-12-05 | 2020-12-06 | 2020-12-07 | 2020-12-08 | 2020-12-09 |
     * 2020-12-10 | 2020-12-11 |
     */
    @Test
    public void testInitAllDatesAvailable() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        Set<LocalDate> dates = bs.getAvailability();
        Assert.isTrue(dates.size() >= 30, "All days of month should be available.");
        print(dates);
    }

    /**
     * POSITIVE: Verify that only EVEN dates are available.
     *
     * 2020-11-12 | 2020-11-14 | 2020-11-16 | 2020-11-18 |...
     */
    @Test
    public void testInitOddDaysNotAvailable() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        Set<LocalDate> dates = bs.getAvailability();
        Assert.isTrue(dates.size() <= 16, "Only even days should be available.");
        for (LocalDate date : dates) {
            Assert.isTrue(date.getDayOfMonth() % 2 == 0, "Only even days should be available: " + date);
        }
        print(dates);
    }

    /**
     * POSITIVE: User books available date.
     */
    @Test
    public void testBookAvailableDate() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        Assert.isTrue(bs.getAvailability().contains(getEvenDay(DAYS_FROM_NOW)), "Even day should be available.");
        Reservation r = bs.create(EMAIL_VALID, NAME_VALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
        Assert.notNull(r, "Reservation should be created.");
        Assert.isTrue(!bs.getAvailability().contains(getEvenDay(DAYS_FROM_NOW)), "Reservation check in date should should NOT be available: ");
    }

    /**
     * NEGATIVE: User attempts to create reservation with a time interval with unavailable date.
     */
    @Test
    public void testBookUnavailableDate() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        LocalDate oddDay = getOddDay(DAYS_FROM_NOW);
        Assert.isTrue(!bs.getAvailability().contains(oddDay), "Odd day should NOT be unavailable.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(String.format(BookingService.ERROR_UNAVAILABLE_DATE, oddDay));
        bs.create(EMAIL_VALID, NAME_VALID, oddDay, oddDay.plusDays(1));
    }

    /**
     * NEGATIVE: User attempts to create reservation with a range exceeding 3 days.
     */
    @Test
    public void testBookWideRange() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate oddDay = getOddDay(DAYS_FROM_NOW);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ERROR_DEPARTURE_RANGE);
        bs.create(EMAIL_VALID, NAME_VALID, oddDay, oddDay.plusDays(4));
    }

    /**
     * NEGATIVE: User attempts to create reservation with arrival equal to today.
     */
    @Test
    public void testBookArrivalEqualsToday() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate today = getToday();
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ERROR_ARRIVAL_RANGE);
        bs.create(EMAIL_VALID, NAME_VALID, today, today.plusDays(1));
    }

    /**
     * NEGATIVE: User attempts to create reservation with arrival later than 1 month.
     */
    @Test
    public void testBookArrivalTooLate() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate monthLater = getToday().plusMonths(1).plusDays(1);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ERROR_ARRIVAL_RANGE);
        bs.create(EMAIL_VALID, NAME_VALID, monthLater, monthLater.plusDays(1));
    }

    /**
     * NEGATIVE: User attempts to create reservation with arrival equal to departure.
     */
    @Test
    public void testBookDepartureEqualsArrival() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate oddDay = getOddDay(DAYS_FROM_NOW);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ERROR_DEPARTURE_RANGE);
        bs.create(EMAIL_VALID, NAME_VALID, oddDay, oddDay);
    }

    /**
     * NEGATIVE: User attempts to create reservation with NULL arrival.
     */
    @Test
    public void testBookNullArrival() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate oddDay = getOddDay(DAYS_FROM_NOW);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ARG_ARRIVAL + BookingService.ERROR_DATE_IS_NULL);
        bs.create(EMAIL_VALID, NAME_VALID, null, oddDay);
    }

    /**
     * NEGATIVE: User attempts to create reservation with NULL departure.
     */
    @Test
    public void testBookNullDeparture() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate oddDay = getOddDay(DAYS_FROM_NOW);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ARG_DEPARTURE + BookingService.ERROR_DATE_IS_NULL);
        bs.create(EMAIL_VALID, NAME_VALID, oddDay, null);
    }

    /**
     * NEGATIVE: create reservation with invalid email.
     */
    @Test
    public void testBookInvalidEmail() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(String.format(BookingService.ERROR_INVALID_EMAIL, EMAIL_INVALID));
        Reservation r = bs.create(EMAIL_INVALID, NAME_VALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
    }

    /**
     * NEGATIVE: create reservation with invalid email.
     */
    @Test
    public void testBookNullEmail() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ARG_EMAIL + BookingService.ERROR_STRING_IS_NULL_OR_EMPTY);
        Reservation r = bs.create(null, NAME_VALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
    }

    /**
     * NEGATIVE: create reservation with EMPTY name.
     */
    @Test
    public void testBookEmptyName() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ARG_NAME + BookingService.ERROR_STRING_IS_NULL_OR_EMPTY);
        bs.create(EMAIL_VALID, NAME_INVALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
    }

    /**
     * NEGATIVE: create reservation with NULL name.
     */
    @Test
    public void testBookNullName() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(BookingService.ARG_NAME + BookingService.ERROR_STRING_IS_NULL_OR_EMPTY);
        bs.create(EMAIL_VALID, null, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
    }

    /**
     * NEGATIVE: update unexisting reservation.
     */
    @Test
    public void testUpdateInvalidReservation() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        String id = Helper.getRandomId();
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(String.format(BookingService.ERROR_NO_RESERVATION_WITH_ID, id));
        bs.update(id, EMAIL_VALID, NAME_VALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
    }

    /**
     * POSITIVE: update existing reservation.
     */
    @Test
    public void testUpdateValidReservation() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        LocalDate date = getEvenDay(DAYS_FROM_NOW);
        Reservation r = bs.create(EMAIL_VALID, NAME_VALID, date, date.plusDays(1));
        Assert.notNull(r, "Reservation should be created.");
        Assert.isTrue(!bs.getAvailability().contains(date), "Reservation's arrival date should NOT be available.");
        bs.update(r.getId(), r.getEmail(), r.getName(), date.plusDays(1), date.plusDays(2));
        Assert.isTrue(bs.getAvailability().contains(date), "Updated reservation's OLD arrival date should be be available.");
        Assert.isTrue(!bs.getAvailability().contains(date.plusDays(1)), "Updated reservation's NEW arrival date should NOT be available.");
    }

    /**
     * NEGATIVE: cancel unexisting reservation.
     */
    @Test
    public void testCancelInvalidReservation() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        String id = Helper.getRandomId();
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(String.format(BookingService.ERROR_NO_RESERVATION_WITH_ID, id));
        bs.cancel(id);
    }

    /**
     * POSITIVE: cancel existing reservation.
     */
    @Test
    public void testCancelValidReservation() {
        BookingService bs = new BookingServiceImpl(dbOddDaysReservation);
        Reservation r = bs.create(EMAIL_VALID, NAME_VALID, getEvenDay(DAYS_FROM_NOW), getEvenDay(DAYS_FROM_NOW).plusDays(1));
        bs.cancel(r.getId());
    }

    /**
     * Demonstrate with appropriate test cases that the system can gracefully handle
     * concurrent requests to reserve the campsite.
     *
     * Output:
     *
     * t1: id: f6d58652-02f5-48f7-a4c7-dacc677c088d | email: email1@gmail.com | name: User1 | arrival: 2020-11-14 | departure: 2020-11-15
     * t25: id: 7026a412-e534-484a-af89-c1f16f5a88d7 | email: email25@gmail.com | name: User25 | arrival: 2020-12-08 | departure: 2020-12-09
     * t10: id: 307f95f4-6463-458e-bc3d-6c983b7fce06 | email: email10@gmail.com | name: User10 | arrival: 2020-11-23 | departure: 2020-11-24
     * ....
     * t24: id: 85a67491-5c46-465f-bf42-c3427887ed92 | email: email24@gmail.com | name: User24 | arrival: 2020-12-07 | departure: 2020-12-08
     * t3: id: 7d323e7c-214a-4e27-931c-8b65012ccfa9 | email: email3@gmail.com | name: User3 | arrival: 2020-11-16 | departure: 2020-11-17
     * t18: id: 75d6f7f5-5146-47d4-a0bd-935bf56a1273 | email: email18@gmail.com | name: User18 | arrival: 2020-12-01 | departure: 2020-12-02
     */
    @Test
    public void testConcurrentBookAndCancel() {
        BookingService bs = new BookingServiceImpl(dbNoReservations);
        int NUM_THREADS = 30;

        // 1. Create reservations in parallel
        CountDownLatch latch = new CountDownLatch(NUM_THREADS);
        List<TestThread> testThreads = new ArrayList();
        for(int i = 0; i < NUM_THREADS; i++) {
            TestThread testThread = new BookThread(i, bs, latch);
            testThreads.add(testThread);
            new Thread(testThread).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {

        }
        Assert.isTrue(bs.getAvailability().size() == 0, "There should not be days available.");
        for(TestThread t: testThreads) {
            Assert.notNull(t.getReservation(), "All reservations should be created.");
            Assert.isTrue(!bs.getAvailability().contains(t.getReservation().getArrival()), "Reserved days can't be available.");
        }

        // 2. Cancel reservations in parallel
        latch = new CountDownLatch(NUM_THREADS);
        List<TestThread> cancelThreads = new ArrayList();
        for(int i = 0; i < NUM_THREADS; i++) {
            TestThread cancelThread = new CancelThread(i, bs, latch, testThreads.get(i).reservation);
            cancelThreads.add(cancelThread);
            new Thread(cancelThread).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {

        }
        Assert.isTrue(bs.getAvailability().size() == NUM_THREADS, "All dates should become available.");
        int i = NUM_THREADS;
        for(TestThread t: testThreads) {
            try {
                bs.get(t.getReservation().getId());
            } catch (IllegalArgumentException e) {
                i--;
            }
        }
        Assert.isTrue(i == 0, "All reservations must be cancelled.");
    }

    /**
     * This thread books reservation.
     */
    private final class BookThread extends TestThread {

        public BookThread(int i, BookingService bookingService, CountDownLatch latch) {
            super(i, bookingService, latch);
        }

        @Override
        protected void useBookingService() {
            LocalDate start = getToday().plusDays(i + 1);
            LocalDate end = getToday().plusDays(i + 2);
            reservation = bookingService.create("email" + i + "@gmail.com", "User" + i, start, end);
            print(reservation, i);
        }
    }

    /**
     * This thread cancels reservation.
     */
    private final class CancelThread extends TestThread {

        public CancelThread(int i, BookingService bookingService, CountDownLatch latch, Reservation reservation) {
            super(i, bookingService, latch);
            this.reservation = reservation;
        }

        @Override
        protected void useBookingService() {
            bookingService.cancel(reservation.getId());
        }
    }

    private abstract class TestThread implements Runnable {

        protected final int i;
        protected final BookingService bookingService;
        protected final CountDownLatch latch;
        protected Reservation reservation;

        public TestThread(int i, BookingService bookingService, CountDownLatch latch) {
            this.i = i;
            this.bookingService = bookingService;
            this.latch = latch;
        }

        @Override
        public void run() {
            randomSleep();
            useBookingService();
            latch.countDown();
        }

        public Reservation getReservation() {
            return reservation;
        }

        public int getI() {
            return i;
        }

        /**
         * Override this method to define the step.
         */
        protected abstract void useBookingService();

        /**
         * Use this method to randomize parallel execution.
         */
        private void randomSleep() {
            try {
                Thread.sleep((long) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private void print(Set<LocalDate> dates) {
        for (LocalDate d : dates) {
            System.out.print(d.toString() + " | ");
        }
    }

    private void print(Reservation r, int i) {
        System.out.println("t" + i + ": " + r.toString());
    }

    private LocalDate getToday() {
        ZoneId zoneId = ZoneId.of(TIMEZONE);
        return LocalDate.now(zoneId);
    }

    private LocalDate getEvenDay(int daysFromToday) {
        ZoneId zoneId = ZoneId.of(TIMEZONE);
        LocalDate candidate = LocalDate.now(zoneId);
        return candidate.plusDays(daysFromToday).getDayOfMonth() % 2 == 0 ? candidate.plusDays(daysFromToday) : candidate.plusDays(daysFromToday + 1);
    }

    private LocalDate getOddDay(int daysFromToday) {
        ZoneId zoneId = ZoneId.of(TIMEZONE);
        LocalDate candidate = LocalDate.now(zoneId);
        return candidate.plusDays(daysFromToday).getDayOfMonth() % 2 == 1 ? candidate.plusDays(daysFromToday) : candidate.plusDays(daysFromToday + 1);
    }
}
